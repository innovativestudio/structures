#include "basicDefines_A3.hpp"
#define _ARMA_

class CfgPatches
{
	class AUS92_Structures_F
	{
		units[] = {};
		requiredVersion = 0.1;
		weapons[] = {};
		requiredAddons[] = {};
	};
};
class CfgVehicleClasses
{
	class AUS92_Structures_F
	{
		displayName = "AUS92 - Structures";
	};
};
class CfgVehicles {};