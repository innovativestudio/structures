#include "basicDefines_A3.hpp"

class CfgPatches {
	class Land_AUS92_Structures_F_FireStation_01 {
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
    };
};
class CfgFactionClasses {
	class Land_AUS92_Structures_F {
		displayName = "AUS92 - Australian Life";
		priority = 100;
		side = 1;
	};
};
class CfgVehicleClasses {
	class Land_AUS92_Structures_F_FireStation {
		displayName = "AUS92 - Fire Stations";
	};
};
class CfgVehicles {
	class All;
	class Static : All {};
	class Building : Static {};
	class NonStrategic : Building {};
    class Land_AUS92_FireStation_01 : NonStrategic {
        scope = 2;
        displayName= "Fire Station 01";
        VehicleClass = "Land_AUS92_Structures_F_FireStation";
        faction = "Land_AUS92_Structures_F";
        model = "\aus92\aus92_structures_f\firestation_01\fireStation_01.p3d";
        placement = vertical;
        mapSize = 3;
        cost = 0;
        armor = 2500;
        class AnimationSources {
            class Door01 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Door02 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Door03 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor01 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor02 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor03 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor04 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor05 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor06 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor07 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor08 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor09 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor10 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor11 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
        };
        class UserActions {
            class OpenDoors01 {
                displayName = "Open Door";
                position = "pos_door01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door01"" < 0.5";
                statement = "this animate [""door01"", 1]";
            };
            class CloseDoors01 {
                displayName = "Close Door";
                position = "pos_door01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door01"" >= 0.5";
                statement = "this animate [""door01"", 0]";
            };
            class OpenDoors02 {
                displayName = "Open Door";
                position = "pos_door02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door02"" < 0.5";
                statement = "this animate [""door02"", 1]";
            };
            class CloseDoors02 {
                displayName = "Close Door";
                position = "pos_door02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door02"" >= 0.5";
                statement = "this animate [""door02"", 0]";
            };
            class OpenDoors03 {
                displayName = "Open Door";
                position = "pos_door03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door03"" < 0.5";
                statement = "this animate [""door03"", 1]";
            };
            class CloseDoors03 {
                displayName = "Close Door";
                position = "pos_door03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door03"" >= 0.5";
                statement = "this animate [""door03"", 0]";
            };
            class OpenDoors04 {
                displayName = "Open Door";
                position = "pos_garageDoor01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor01"" < 0.5";
                statement = "this animate [""garageDoor01"", 1]";
            };
            class CloseDoors04 {
                displayName = "Close Door";
                position = "pos_garageDoor01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor01"" >= 0.5";
                statement = "this animate [""garageDoor01"", 0]";
            };
            class OpenDoors05 {
                displayName = "Open Door";
                position = "pos_garageDoor02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor02"" < 0.5";
                statement = "this animate [""garageDoor02"", 1]";
            };
            class CloseDoors05 {
                displayName = "Close Door";
                position = "pos_garageDoor02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor02"" >= 0.5";
                statement = "this animate [""garageDoor02"", 0]";
            };
            class OpenDoors06 {
                displayName = "Open Door";
                position = "pos_garageDoor03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor03"" < 0.5";
                statement = "this animate [""garageDoor03"", 1]";
            };
            class CloseDoors06 {
                displayName = "Close Door";
                position = "pos_garageDoor03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor03"" >= 0.5";
                statement = "this animate [""garageDoor03"", 0]";
            };
            class OpenDoors07 {
                displayName = "Open Door";
                position = "pos_garageDoor04";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor04"" < 0.5";
                statement = "this animate [""garageDoor04"", 1]";
            };
            class CloseDoors07 {
                displayName = "Close Door";
                position = "pos_garageDoor04";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor04"" >= 0.5";
                statement = "this animate [""garageDoor04"", 0]";
            };
            class OpenDoors08 {
                displayName = "Open Door";
                position = "pos_garageDoor05";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor05"" < 0.5";
                statement = "this animate [""garageDoor05"", 1]";
            };
            class CloseDoors08 {
                displayName = "Close Door";
                position = "pos_garageDoor05";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor05"" >= 0.5";
                statement = "this animate [""garageDoor05"", 0]";
            };
            class OpenDoors09 {
                displayName = "Open Door";
                position = "pos_garageDoor06";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor06"" < 0.5";
                statement = "this animate [""garageDoor06"", 1]";
            };
            class CloseDoors09 {
                displayName = "Close Door";
                position = "pos_garageDoor06";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor06"" >= 0.5";
                statement = "this animate [""garageDoor06"", 0]";
            };
            class OpenDoors10 {
                displayName = "Open Door";
                position = "pos_garageDoor07";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor07"" < 0.5";
                statement = "this animate [""garageDoor07"", 1]";
            };
            class CloseDoors10 {
                displayName = "Close Door";
                position = "pos_garageDoor07";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor07"" >= 0.5";
                statement = "this animate [""garageDoor07"", 0]";
            };
            class OpenDoors11 {
                displayName = "Open Door";
                position = "pos_garageDoor08";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor08"" < 0.5";
                statement = "this animate [""garageDoor08"", 1]";
            };
            class CloseDoors11 {
                displayName = "Close Door";
                position = "pos_garageDoor08";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor08"" >= 0.5";
                statement = "this animate [""garageDoor08"", 0]";
            };
            class OpenDoors12 {
                displayName = "Open Door";
                position = "pos_garageDoor09";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor09"" < 0.5";
                statement = "this animate [""garageDoor09"", 1]";
            };
            class CloseDoors12 {
                displayName = "Close Door";
                position = "pos_garageDoor09";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor09"" >= 0.5";
                statement = "this animate [""garageDoor09"", 0]";
            };
            class OpenDoors13 {
                displayName = "Open Door";
                position = "pos_garageDoor10";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor10"" < 0.5";
                statement = "this animate [""garageDoor10"", 1]";
            };
            class CloseDoors13 {
                displayName = "Close Door";
                position = "pos_garageDoor10";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor10"" >= 0.5";
                statement = "this animate [""garageDoor10"", 0]";
            };
            class OpenDoors14 {
                displayName = "Open Door";
                position = "pos_garageDoor11";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor11"" < 0.5";
                statement = "this animate [""garageDoor11"", 1]";
            };
            class CloseDoors14 {
                displayName = "Close Door";
                position = "pos_garageDoor11";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor11"" >= 0.5";
                statement = "this animate [""garageDoor11"", 0]";
            };
        };
        class EventHandlers {
            init = "";
        };
    };
};