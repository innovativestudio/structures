#include "basicDefines_A3.hpp"

class CfgPatches {
	class Land_AUS92_Structures_F_Firehouse_01 {
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
    };
};
class CfgFactionClasses {
	class Land_AUS92_Structures_F {
		displayName = "AUS92 - Australian Life";
		priority = 100;
		side = 1;
	};
};
class CfgVehicleClasses {
	class Land_AUS92_Structures_F_Firehouse {
		displayName = "AUS92 - Firehouses";
	};
};
class CfgVehicles {
	class All;
	class Static : All {};
	class Building : Static {};
	class NonStrategic : Building {};
    class Land_AUS92_Firehouse_01 : NonStrategic {
        scope = 2;
        displayName= "Firehouse 01";
        VehicleClass = "Land_AUS92_Structures_F_Firehouse";
        faction = "Land_AUS92_Structures_F";
        model = "\aus92\aus92_structures_f\firehouse_01\firehouse_01.p3d";
        placement = vertical;
        mapSize = 3;
        cost = 0;
        armor = 2500;
        class AnimationSources {
            class Door01 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor01 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor02 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor03 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class garageDoor04 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
        };
        class UserActions {
            class OpenDoors01 {
                displayName = "Open Door";
                position = "pos_door01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door01"" < 0.5";
                statement = "this animate [""door01"", 1]";
            };
            class CloseDoors01 {
                displayName = "Close Door";
                position = "pos_door01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door01"" >= 0.5";
                statement = "this animate [""door01"", 0]";
            };
            class OpenDoors02 {
                displayName = "Open Door";
                position = "pos_garageDoor01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor01"" < 0.5";
                statement = "this animate [""garageDoor01"", 1]";
            };
            class CloseDoors02 {
                displayName = "Close Door";
                position = "pos_garageDoor01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor01"" >= 0.5";
                statement = "this animate [""garageDoor01"", 0]";
            };
            class OpenDoors03 {
                displayName = "Open Door";
                position = "pos_garageDoor02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor02"" < 0.5";
                statement = "this animate [""garageDoor02"", 1]";
            };
            class CloseDoors03 {
                displayName = "Close Door";
                position = "pos_garageDoor02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor02"" >= 0.5";
                statement = "this animate [""garageDoor02"", 0]";
            };
            class OpenDoors04 {
                displayName = "Open Door";
                position = "pos_garageDoor03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor03"" < 0.5";
                statement = "this animate [""garageDoor03"", 1]";
            };
            class CloseDoors04 {
                displayName = "Close Door";
                position = "pos_garageDoor03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor03"" >= 0.5";
                statement = "this animate [""garageDoor03"", 0]";
            };
            class OpenDoors05 {
                displayName = "Open Door";
                position = "pos_garageDoor04";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor04"" < 0.5";
                statement = "this animate [""garageDoor04"", 1]";
            };
            class CloseDoors05 {
                displayName = "Close Door";
                position = "pos_garageDoor04";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""garageDoor04"" >= 0.5";
                statement = "this animate [""garageDoor04"", 0]";
            };
        };
		class EventHandlers {
			init = "";
		};
	};
};