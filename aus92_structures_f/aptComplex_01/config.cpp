#include "basicDefines_A3.hpp"

class CfgPatches {
	class Land_AUS92_Structures_F_aptComplex_01 {
		units[] = {};
		weapons[] = {};
		requiredAddons[] = {};
		requiredVersion = 0.1;
	};
};
class CfgFactionClasses {
	class Land_AUS92_Structures_F {
		displayName = "AUS92 - Australian Life";
		priority = 100;
		side = 1;
	};
};
class CfgVehicleClasses {
	class Land_AUS92_Structures_F_Apartment {
		displayName = "AUS92 - Apartments";
	};
};
class CfgVehicles {
	class All;
	class Static : All {};
	class Building : Static {};
	class NonStrategic : Building {};
	class Land_AUS92_aptComplex_01: NonStrategic {
		mapSize = 8;
		author = "J. Schmidt";
		_generalMacro = "";
		scope = 2;
		displayName = "Apartment 01";
		model = "\aus92\aus92_structures_f\aptComplex_01\aptComplex_01.p3d";
		vehicleClass = "Land_AUS92_Structures_F_Apartment";
		faction = "Land_AUS92_Structures_F";
		cost = 5000;
	};
};