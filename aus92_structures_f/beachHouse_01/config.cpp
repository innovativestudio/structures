#include "config_macros_glass.hpp"
#include "basicDefines_A3.hpp"

class cfgPatches {
	class Land_AUS92_Structures_F_BeachHouse_01 {
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
    };
};
class cfgFactionClasses {
	class Land_AUS92_Structures_F {
		displayName = "AUS92 - Australian Life";
		priority = 100;
		side = "none";
	};
};
class cfgVehicleClasses {
	class Land_AUS92_Structures_F_BeachHouse {
		displayName = "AUS92 - Beach Houses";
	};
};
class cfgVehicles {
	class All;
	class Static : All {};
	class Building : Static {};
	class NonStrategic : Building {};
    class Land_AUS92_BeachHouse_01 : NonStrategic {
        scope = 2;
        displayName= "Beach House 01";
        VehicleClass = "Land_AUS92_Structures_F_BeachHouse";
        faction = "Land_AUS92_Structures_F";
        model = "\aus92\aus92_structures_f\beachHouse_01\beachHouse_01.p3d";
        placement = vertical;
        mapSize = 3;
        cost = 0;
        armor = 2500;
        class HitPoints // Entities representing destructible subparts of the house
        {
            class Hitzone_1_hitpoint
            {
                armor = 20;
                material = -1;
                name = Dam_1; // Name of selection in Hit-points lod in p3d
                visual = DamT_1; // Name of selection in resolution lods in p3d that will have it's textures and materials switched (according to "class Damage definitions") based on damage of this hitpoint
                passThrough = 1.0; // Coefficient for how much damage done to this hitpoints is also done to total damage of the house
                radius = 0.375; // Radius of spheres around each vertex of this hitpoint in Hit-points lod. These spheres represent the volume from which this hitpoint takes damage
                convexComponent = Dam_2;
                explosionShielding = 50; // Multiplier for damage taken from explosives
                minimalHit = 0.001; // Minimal damage that can be dealt to the hitpoint. Any lower damage is ignored
                class DestructionEffects // 
                {
                    class Dust
                    {
                        simulation = particles; // Visual effect
                        type = HousePartDust; // Class of this particular effect, defined in CfgCloudlets
                        position = Dam_1_effects; // Point of origin for this effect, defined in Memory lod in p3d
                        intensity = 1;
                        interval = 1;
                        lifeTime = 0.01;
                    };
                    class Dust2: Dust
                    {
                        type = HousePartDustLong;
                    };
                    class Walls: Dust
                    {
                        type = HousePartWall;
                    };
                    class DamageAround
                    {
                        simulation = damageAround; // Effect dealing damage in a radius
                        type = DamageAroundHousePart; // Class of this particular effect, defined in CfgDamageAround
                        position = Dam_1_effects;
                        intensity = 1;
                        interval = 1;
                        lifeTime = 1;
                    };
                };
            };
            class Hitzone_2_hitpoint: Hitzone_1_hitpoint
            {
                name = Dam_2;
                convexComponent = Dam_2;

                class DestructionEffects: DestructionEffects
                {
                    class Dust: Dust
                    {
                        position = Dam_2_effects;
                    };
                    class Dust2: Dust2
                    {
                        position = Dam_2_effects;
                    };
                    class Walls: Walls
                    {
                        position = Dam_2_effects;
                    };
                    class DamageAround: DamageAround
                    {
                        position = Dam_2_effects;
                    };
                };
            };

            // Hitpoint of each window, defined using macros from config_macros_glass.hpp to avoid a giant wall of text due to having 14 particle effects each.
            // In practice they are defined in the same manner as the hitpoints above. These follow Glass_#_hitpoint naming trend.
            // First parameter being number id, second being a value for armor parameter and third being a value for radius parameter.
            NORMAL_GLASS_HITPOINT(1,0.01,0.175)
            NORMAL_GLASS_HITPOINT(2,0.01,0.175)
            NORMAL_GLASS_HITPOINT(3,0.01,0.175)
            NORMAL_GLASS_HITPOINT(4,0.01,0.175)
            NORMAL_GLASS_HITPOINT(5,0.01,0.175)
            NORMAL_GLASS_HITPOINT(6,0.01,0.175)
            NORMAL_GLASS_HITPOINT(7,0.01,0.175)
            NORMAL_GLASS_HITPOINT(8,0.01,0.175)
            NORMAL_GLASS_HITPOINT(9,0.01,0.175)
            NORMAL_GLASS_HITPOINT(10,0.01,0.175)
            NORMAL_GLASS_HITPOINT(11,0.01,0.175)
            NORMAL_GLASS_HITPOINT(12,0.01,0.175)
            NORMAL_GLASS_HITPOINT(13,0.01,0.175)
            NORMAL_GLASS_HITPOINT(14,0.01,0.175)
            NORMAL_GLASS_HITPOINT(15,0.01,0.175)
            NORMAL_GLASS_HITPOINT(16,0.01,0.175)
            NORMAL_GLASS_HITPOINT(17,0.01,0.175)
            NORMAL_GLASS_HITPOINT(18,0.01,0.175)
            NORMAL_GLASS_HITPOINT(19,0.01,0.175)
            NORMAL_GLASS_HITPOINT(20,0.01,0.175)
            NORMAL_GLASS_HITPOINT(21,0.01,0.175)
            NORMAL_GLASS_HITPOINT(22,0.01,0.175)
            NORMAL_GLASS_HITPOINT(23,0.01,0.175)
            NORMAL_GLASS_HITPOINT(24,0.01,0.175)
            NORMAL_GLASS_HITPOINT(25,0.01,0.175)
            NORMAL_GLASS_HITPOINT(26,0.01,0.175)
            NORMAL_GLASS_HITPOINT(27,0.01,0.175)
            NORMAL_GLASS_HITPOINT(28,0.01,0.175)
            NORMAL_GLASS_HITPOINT(29,0.01,0.175)
            NORMAL_GLASS_HITPOINT(30,0.01,0.175)
            NORMAL_GLASS_HITPOINT(31,0.01,0.175)
            NORMAL_GLASS_HITPOINT(32,0.01,0.175)
            NORMAL_GLASS_HITPOINT(33,0.01,0.175)
            NORMAL_GLASS_HITPOINT(34,0.01,0.175)
            NORMAL_GLASS_HITPOINT(35,0.01,0.175)
            NORMAL_GLASS_HITPOINT(36,0.01,0.175)
            NORMAL_GLASS_HITPOINT(37,0.01,0.175)
            NORMAL_GLASS_HITPOINT(38,0.01,0.175)
            NORMAL_GLASS_HITPOINT(39,0.01,0.175)
            NORMAL_GLASS_HITPOINT(40,0.01,0.175)
            NORMAL_GLASS_HITPOINT(41,0.01,0.175)
            NORMAL_GLASS_HITPOINT(42,0.01,0.175)
            NORMAL_GLASS_HITPOINT(43,0.01,0.175)
            NORMAL_GLASS_HITPOINT(44,0.01,0.175)
        };
        class Damage
        {
            // Texture pairs (below 0.5 health and 0.5+) for switching visuals (can also use generated) 
            tex[] =
            {
                // Window textures
                "A3\Structures_F\Data\Windows\window_set_CA.paa",
                "A3\Structures_F\Data\Windows\destruct_half_window_set_CA.paa",

                // Grey color
                "#(argb,8,8,3)color(0.501961,0.501961,0.501961,1.0,co)",
                "#(argb,8,8,3)color(0.294118,0.294118,0.294118,1.0,co)",

                // Brown color
                "#(argb,8,8,3)color(0.501961,0.25098,0,1.0,co)",
                "#(argb,8,8,3)color(0.392157,0.196078,0,1.0,co)",

                // Yellow color
                "#(argb,8,8,3)color(1,1,0.501961,1.0,co)",
                "#(argb,8,8,3)color(0.513725,0.513725,0.203922,1.0,co)",

                // Light grey color
                "#(argb,8,8,3)color(0.752941,0.752941,0.752941,1.0,co)",
                "#(argb,8,8,3)color(0.478431,0.478431,0.478431,1.0,co)",

                // Red color
                "#(argb,8,8,3)color(1,0,0,1.0,co)",
                "#(argb,8,8,3)color(0.701961,0,0,1.0,co)"
            };

            // Unlike textures, materials are not in pairs but in triplets (health: 0 - 0.49, 0.5 - 0.99, 1)
            mat[] =
            {               
                "A3\Structures_F\Data\Windows\window_set.rvmat",
                "A3\Structures_F\Data\Windows\destruct_half_window_set.rvmat",
                "A3\Structures_F\Data\Windows\destruct_full_window_set.rvmat"
            };
        };
        class AnimationSources {
            class Door01 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Door02 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Door03 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Door04 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Door05 {
                source = "user";
                animPeriod = 1;
                initPhase = 0;
            };
            class Glass_1_source
            {
                source = Hit; // "Hit" = value of this source is the health of an entity
                hitpoint = Glass_1_hitpoint; // Specifies health of what is the control value of this animation; "Glass_1_hitpoint" being the class defined in class Hitpoints
                raw = 1;
            };
            class Glass_2_source: Glass_1_source
            {
                hitpoint = Glass_2_hitpoint;
            };
            class Glass_3_source: Glass_1_source
            {
                hitpoint = Glass_3_hitpoint;
            };
            class Glass_4_source: Glass_1_source
            {
                hitpoint = Glass_4_hitpoint;
            };
            class Glass_5_source: Glass_1_source
            {
                hitpoint = Glass_5_hitpoint;
            };
            class Glass_6_source: Glass_1_source
            {
                hitpoint = Glass_6_hitpoint;
            };
            class Glass_7_source: Glass_1_source
            {
                hitpoint = Glass_7_hitpoint;
            };
            class Glass_8_source: Glass_1_source
            {
                hitpoint = Glass_8_hitpoint;
            };
            class Glass_9_source: Glass_1_source
            {
                hitpoint = Glass_9_hitpoint;
            };
            class Glass_10_source: Glass_1_source
            {
                hitpoint = Glass_10_hitpoint;
            };
            class Glass_11_source: Glass_1_source
            {
                hitpoint = Glass_11_hitpoint;
            };
            class Glass_12_source: Glass_1_source
            {
                hitpoint = Glass_12_hitpoint;
            };
            class Glass_13_source: Glass_1_source
            {
                hitpoint = Glass_13_hitpoint;
            };
            class Glass_14_source: Glass_1_source
            {
                hitpoint = Glass_14_hitpoint;
            };
            class Glass_15_source: Glass_1_source
            {
                hitpoint = Glass_15_hitpoint;
            };
            class Glass_16_source: Glass_1_source
            {
                hitpoint = Glass_16_hitpoint;
            };
            class Glass_17_source: Glass_1_source
            {
                hitpoint = Glass_17_hitpoint;
            };
            class Glass_18_source: Glass_1_source
            {
                hitpoint = Glass_18_hitpoint;
            };
            class Glass_19_source: Glass_1_source
            {
                hitpoint = Glass_19_hitpoint;
            };
            class Glass_20_source: Glass_1_source
            {
                hitpoint = Glass_20_hitpoint;
            };
            class Glass_21_source: Glass_1_source
            {
                hitpoint = Glass_21_hitpoint;
            };
            class Glass_22_source: Glass_1_source
            {
                hitpoint = Glass_22_hitpoint;
            };
            class Glass_23_source: Glass_1_source
            {
                hitpoint = Glass_23_hitpoint;
            };
            class Glass_24_source: Glass_1_source
            {
                hitpoint = Glass_24_hitpoint;
            };
            class Glass_25_source: Glass_1_source
            {
                hitpoint = Glass_25_hitpoint;
            };
            class Glass_26_source: Glass_1_source
            {
                hitpoint = Glass_26_hitpoint;
            };
            class Glass_27_source: Glass_1_source
            {
                hitpoint = Glass_27_hitpoint;
            };
            class Glass_28_source: Glass_1_source
            {
                hitpoint = Glass_28_hitpoint;
            };
            class Glass_29_source: Glass_1_source
            {
                hitpoint = Glass_29_hitpoint;
            };
            class Glass_30_source: Glass_1_source
            {
                hitpoint = Glass_30_hitpoint;
            };
            class Glass_31_source: Glass_1_source
            {
                hitpoint = Glass_31_hitpoint;
            };
            class Glass_32_source: Glass_1_source
            {
                hitpoint = Glass_32_hitpoint;
            };
            class Glass_33_source: Glass_1_source
            {
                hitpoint = Glass_33_hitpoint;
            };
            class Glass_34_source: Glass_1_source
            {
                hitpoint = Glass_34_hitpoint;
            };
            class Glass_35_source: Glass_1_source
            {
                hitpoint = Glass_35_hitpoint;
            };
            class Glass_36_source: Glass_1_source
            {
                hitpoint = Glass_36_hitpoint;
            };
            class Glass_37_source: Glass_1_source
            {
                hitpoint = Glass_37_hitpoint;
            };
            class Glass_38_source: Glass_1_source
            {
                hitpoint = Glass_38_hitpoint;
            };
            class Glass_39_source: Glass_1_source
            {
                hitpoint = Glass_39_hitpoint;
            };
            class Glass_40_source: Glass_1_source
            {
                hitpoint = Glass_40_hitpoint;
            };
            class Glass_41_source: Glass_1_source
            {
                hitpoint = Glass_41_hitpoint;
            };
            class Glass_42_source: Glass_1_source
            {
                hitpoint = Glass_42_hitpoint;
            };
            class Glass_43_source: Glass_1_source
            {
                hitpoint = Glass_43_hitpoint;
            };
            class Glass_44_source: Glass_1_source
            {
                hitpoint = Glass_44_hitpoint;
            };
        };
        class UserActions {
            class OpenDoors01 {
                displayName = "Open Door";
                position = "pos_door01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door01"" < 0.5";
                statement = "this animate [""door01"", 1]";
            };
            class CloseDoors01 {
                displayName = "Close Door";
                position = "pos_door01";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door01"" >= 0.5";
                statement = "this animate [""door01"", 0]";
            };
            class OpenDoors02 {
                displayName = "Open Door";
                position = "pos_door02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door02"" < 0.5";
                statement = "this animate [""door02"", 1]";
            };
            class CloseDoors02 {
                displayName = "Close Door";
                position = "pos_door02";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door02"" >= 0.5";
                statement = "this animate [""door02"", 0]";
            };
            class OpenDoors03 {
                displayName = "Open Door";
                position = "pos_door03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door03"" < 0.5";
                statement = "this animate [""door03"", 1]";
            };
            class CloseDoors03 {
                displayName = "Close Door";
                position = "pos_door03";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door03"" >= 0.5";
                statement = "this animate [""door03"", 0]";
            };
            class OpenDoors04 {
                displayName = "Open Door";
                position = "pos_door04";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door04"" < 0.5";
                statement = "this animate [""door04"", 1]";
            };
            class CloseDoors04 {
                displayName = "Close Door";
                position = "pos_door04";
                radius = 3;
                onlyForPlayer = 0;
                condition = "this animationPhase ""door04"" >= 0.5";
                statement = "this animate [""door04"", 0]";
            };            
        };
		class Reflectors {
            class CeilingLight01 {
                ambient[] = {255,255,255};
                brightness = 1;
                color[] = {255,255,255};
                coneFadeCoef = 1;
                dayLight = 0;
                direction = "light01_end";
                flareSize = 1;
                hitpoint = "light01";
                innerAngle = 5;
                setLightIntensity = 1;
                outerAngle = 30;
                position = "light01";
                selection = "light01";
                size = 1;
                useFlare = 0;
                class Attenuation {
                    start = 1;
                    constant = 0;
                    linear = 0;
                    quadratic = 0.25;
                    hardLimitStart = 30;
                    hardLimitEnd = 60;
                };
            };
            class CeilingLight02 {
                ambient[] = {255,255,255};
                brightness = 1;
                color[] = {255,255,255};
                coneFadeCoef = 1;
                dayLight = 0;
                direction = "light02_end";
                flareSize = 1;
                hitpoint = "light02";
                innerAngle = 5;
                intensity = 1;
                outerAngle = 30;
                position = "light02";
                selection = "light02";
                size = 1;
                useFlare = 0;
                class Attenuation {
                    start = 1;
                    constant = 0;
                    linear = 0;
                    quadratic = 0.25;
                    hardLimitStart = 30;
                    hardLimitEnd = 60;
                };
            };
        };
        class EventHandlers {
            init = "";
        };
    };
};