#define NORMAL_GLASS_HITPOINT(glassID,arm,rad) \
			class Glass_##glassID##_hitpoint \
			{ \
				armor = arm; \
				material = -1; \
				name = Glass_##glassID; \
				visual = Glass_##glassID##_hide; \
				passThrough = 0; \
				radius = rad; \
				convexComponent = Glass_##glassID##_hide; \
				class DestructionEffects \
				{ \
					class BrokenGlass1 \
					{ \
						simulation = "particles"; \
						type = "BrokenGlass1NN"; \
						position = Glass_##glassID##_effects; \
						intensity = 0.15000001; \
						interval = 1; \
						lifeTime = 0.05; \
					}; \
					class BrokenGlass2: BrokenGlass1 \
					{ \
						type = "BrokenGlass2NN"; \
					}; \
					class BrokenGlass3: BrokenGlass1 \
					{ \
						type = "BrokenGlass3NN"; \
					}; \
					class BrokenGlass4: BrokenGlass1 \
					{ \
						type = "BrokenGlass4NN"; \
					}; \
					class BrokenGlass5: BrokenGlass1 \
					{ \
						type = "BrokenGlass5NN"; \
					}; \
					class BrokenGlass6: BrokenGlass1 \
					{ \
						type = "BrokenGlass6NN"; \
					}; \
					class BrokenGlass7: BrokenGlass1 \
					{ \
						type = "BrokenGlass7NN"; \
					}; \
					class BrokenGlass1S: BrokenGlass1 \
					{ \
						type = "BrokenGlass1SN"; \
					}; \
					class BrokenGlass2S: BrokenGlass1 \
					{ \
						type = "BrokenGlass2SN"; \
					}; \
					class BrokenGlass3S: BrokenGlass1 \
					{ \
						type = "BrokenGlass3SN"; \
					}; \
					class BrokenGlass4S: BrokenGlass1 \
					{ \
						type = "BrokenGlass4SN"; \
					}; \
					class BrokenGlass5S: BrokenGlass1 \
					{ \
						type = "BrokenGlass5SN"; \
					}; \
					class BrokenGlass6S: BrokenGlass1 \
					{ \
						type = "BrokenGlass6SN"; \
					}; \
					class BrokenGlass7S: BrokenGlass1 \
					{ \
						type = "BrokenGlass7SN"; \
					}; \
				}; \
			};