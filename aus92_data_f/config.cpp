#define _ARMA_

class CfgPatches
{
	class AUS92_Data_F
	{
		units[] = {};
		requiredVersion = 0.1;
		weapons[] = {};
		requiredAddons[] = {};
	};
};
class CfgVehicleClasses
{
	class AUS92_Data_F
	{
		displayName = "AUS92 - Data";
	};
};
class CfgVehicles {};