#define _ARMA_

class CfgPatches
{
	class AUS92_Misc_F_Electronics
	{
		units[] = {};
		requiredVersion = 0.1;
		weapons[] = {};
		requiredAddons[] = {};
	};
};
class CfgVehicleClasses
{
	class AUS92_Misc_F_Electronics
	{
		displayName = "AUS92 - Electronics";
	};
};
class CfgVehicles {};