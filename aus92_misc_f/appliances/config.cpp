#define _ARMA_

class CfgPatches
{
	class AUS92_Misc_F_Appliances
	{
		units[] = {};
		requiredVersion = 0.1;
		weapons[] = {};
		requiredAddons[] = {};
	};
};
class CfgVehicleClasses
{
	class AUS92_Misc_F_Appliances
	{
		displayName = "AUS92 - Appliances";
	};
};
class CfgVehicles {};