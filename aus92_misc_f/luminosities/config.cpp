#define _ARMA_

class CfgPatches
{
	class AUS92_Misc_F_Luminosities
	{
		units[] = {};
		requiredVersion = 0.1;
		weapons[] = {};
		requiredAddons[] = {};
	};
};
class CfgVehicleClasses
{
	class AUS92_Misc_F_Luminosities
	{
		displayName = "AUS92 - Luminosities";
	};
};
class CfgVehicles {};