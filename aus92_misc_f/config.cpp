#define _ARMA_

class CfgPatches
{
	class AUS92_Misc_F
	{
		units[] = {};
		requiredVersion = 0.1;
		weapons[] = {};
		requiredAddons[] = {};
	};
};
class CfgVehicleClasses
{
	class AUS92_Misc_F
	{
		displayName = "AUS92 - Misc";
	};
};
class CfgVehicles {};